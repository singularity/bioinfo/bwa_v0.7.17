#Package bwa version 0.7.17
BWA is a software package for mapping DNA sequences against a large reference genome, such as the human genome. It consists of three algorithms: BWA-backtrack, BWA-SW and BWA-MEM. The first algorithm is designed for Illumina sequence reads up to 100bp, while the rest two for longer sequences ranged from 70bp to a few megabases
bwa Version: 0.7.17
[https://github.com/lh3/bwa]

Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH bwa Version: 0.7.17
Singularity container based on the recipe: Singularity.bwa_v0.7.17
## Local build:
```bash
sudo singularity build bwa_v0.7.17.sif Singularity.bwa_v0.7.17
```
## Get image help:

```bash
singularity run-help bwa_v0.7.17.sif
```
### Default runscript: bwa
## Usage:
```bash
./bwa_v0.7.17.sif --help
```
or:
```bash
singularity exec bwa_v0.7.17.sif bwa --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull bwa_v0.7.17.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/bwa_v0.7.17/bwa_v0.7.17:latest
```

